<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Recipe;
use Storage;

class AdminController extends Controller
{
    public function __construct()
    {
      $this->middleware('admin');
    }

    public function users()
    {
      $users = User::latest()->paginate(15);

      return view('admin.users', compact('users'));
    }

    public function manage()
    {
      $recipes = Recipe::latest()->paginate(15);

      return view('admin.manage', compact('recipes'));
    }

    public function edit(Recipe $recipe)
    {
      return view('edit', compact('recipe'));
    }

    public function update(Recipe $recipe)
    {
      request()->validate([
          'title' => 'required|min:3',
          'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
          'description' => 'required|min:10',
          'ingredients' => 'required|min:5',
          'category' => 'required'
        ]);

        $recipe->title = request('title');
        $recipe->description = request('description');
        $recipe->ingredients = request('ingredients');
        $recipe->category = request('category');

      if (request()->has('image')) {
        $oldImage = $recipe->image;
        $recipe->image = request('image')->store('images');;
        Storage::delete($oldImage);
      }

      $recipe->save();

      return redirect("/recipes/$recipe->id")->with('message', 'Success! Recipe updated!');
    }

    public function destroy(User $user)
    {
      $user->delete();

      return back()->with('message', 'Success! User deleted!');
    }
}
