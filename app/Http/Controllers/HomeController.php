<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Favourite;
use App\Comment;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('welcome');
    }

    public function index($id, $name)
    {
      $favoured = Favourite::where('user_id', $id)->pluck('recipe_id');
      $recipes = Recipe::whereIn('id', $favoured)->paginate(10);
      $comments = Comment::where('user_id', $id)->get();

      return view('home', compact('recipes', 'comments', 'name'));
    }

    public function welcome()
    {
      $count['appetizers'] = Recipe::where('category', 'appetizers')->count();
      $count['salads'] = Recipe::where('category', 'salads')->count();
      $count['soups'] = Recipe::where('category', 'soups')->count();
      $count['main'] = Recipe::where('category', 'main')->count();
      $count['desserts'] = Recipe::where('category', 'desserts')->count();
      $count['sauces'] = Recipe::where('category', 'sauces')->count();

      return view('welcome', compact('count'));
    }
}
