<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Comment;

class CommentController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function store(Recipe $recipe)
    {
      request()->validate([
        'body' => 'required'
      ]);

      Comment::create([
        'body' => request('body'),
        'recipe_id' => $recipe->id,
        'user_id' =>  auth()->id()
      ]);

      return back();
    }
}
