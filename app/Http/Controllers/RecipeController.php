<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Favourite;
use Storage;

class RecipeController extends Controller
{
    public function __construct()
    {
      $this->middleware('admin')->except('index', 'show', 'search');
    }

    public function index()
    {
      if (request()->has('category')) {
        $recipes = Recipe::where('category', request('category'))->latest()->get();
      }else {
        $recipes = Recipe::latest()->get();
      }

      return view('index', compact('recipes'));
    }

    public function show(Recipe $recipe)
    {
      $favoured = Favourite::where('recipe_id', $recipe->id)->where('user_id', auth()->id())->first();

      return view('show', compact('recipe', 'favoured'));
    }

    public function create()
    {
      return view('create');
    }

    public function store()
    {
      request()->validate([
          'title' => 'required|min:3',
          'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          'description' => 'required|min:10',
          'ingredients' => 'required|min:5',
          'category' => 'required'
        ]);

      Recipe::create([
        'title' => request('title'),
        'image' => request('image')->store('images'),
        'description' => request('description'),
        'ingredients' => request('ingredients'),
        'category' => request('category'),
        'user_id' => auth()->id()
      ]);

      return redirect('/recipes')->with('message', 'Success! New recipe posted!');
    }

    public function search()
    {
      if (request()->has('keyword')) {
        $keyword = '%'.request('keyword').'%';

        $recipes = Recipe::where('title', 'LIKE', $keyword)
                        ->orWhere('description', 'LIKE', $keyword)
                        ->orWhere('ingredients', 'LIKE', $keyword)
                        ->orWhere('category', 'LIKE', $keyword)
                        ->get();

        return view('index', compact('recipes'));
      }
    }

    public function destroy(Recipe $recipe)
    {
      Storage::delete($recipe->image);
      $recipe->delete();

      return back()->with('message', 'Success! Recipe deleted!');
    }

}
