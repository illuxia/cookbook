<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favourite;
use App\Recipe;

class FavouriteController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function store(Recipe $recipe)
  {
    Favourite::create([
      'recipe_id' => $recipe->id,
      'user_id' => auth()->id()
    ]);

    return back();
  }

  public function index()
  {
    $favoured = Favourite::where('user_id', auth()->id())->pluck('recipe_id');

    $recipes = Recipe::whereIn('id', $favoured)->get();

    return view('index', compact('recipes'));
  }

  public function destroy(Recipe $recipe)
  {
    Favourite::where('recipe_id', $recipe->id)->where('user_id', auth()->id())->delete();

    return back();
  }

}
