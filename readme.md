After downloading the repository:

1. Set permissions in project directory: sudo chmod -R 777 /project/

2. Inside project directory run the following command to create a link to storage directory in order to see uploaded images: php artisan storage:link

3. Import the demo database (project/demo_database/cookbook.sql) or to start with an empty database run: php artisan:migrate

Project Overview:

Laravel CRUD application for cooking recipes. Database and other environment details can be found in /project/.env file. If you choose to use the demo database, it provides 50 recipes and 3 registered users along with associated comments and favourite recipes. Two of the users are admins and one is a normal user:

Demo users login information:

email - boyan@mail.com password - qwerty

email - geri@mail.com password - qwerty

email - random@mail.com password - qwerty
