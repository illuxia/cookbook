@extends('layouts.app')

@section('content')

<div class = "container">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h4 class="box-title text-center"><strong>Manage recipes</strong></h4>
				</div>
				<hr>
				<div class="box-body">
					<table class="table table-bordered table-hover">
						<thead>
						<tr>
							<th class="text-center">Title</th>
							<th class="text-center">Category</th>
							<th class="text-center">Date added</th>
              <th class="text-center">Author</th>
							<th class="text-center">Actions</th>
						</tr>
						</thead>
						<tbody>
						@foreach($recipes as $recipe)
							<tr>
								<td>{{$recipe->title}}</td>
								<td class="text-center">{{ucwords($recipe->category)}}</td>
								<td class="text-center">{{$recipe->created_at}}</td>
                <td class="text-center">{{$recipe->user->name}}</td>
								<td class="text-center">
                  <a class="btn btn-primary btn-sm" href="/recipes/{{$recipe->id}}">View</a>
                  <a class="btn btn-warning btn-sm" href="/admin/{{$recipe->id}}/edit">Edit</a>
                  <a class="btn btn-danger btn-sm" href="/recipe/{{$recipe->id}}/delete">Delete</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>

					<div class="container text-center">
						<div class="row">
							{{ $recipes->links() }}
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection
