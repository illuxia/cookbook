@extends('layouts.app')

@section('content')

<div class = "container">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h4 class="box-title text-center">Manage Users</h4>
				</div>
				<hr>
				<div class="box-body">
					<table class="table table-bordered table-hover">
						<thead>
						<tr>
							<th class="text-center">Name</th>
							<th class="text-center">Role</th>
							<th class="text-center">Email</th>
							<th class="text-center">Registered</th>
							<th class="text-center">Actions</th>
						</tr>
						</thead>
						<tbody>
						@foreach($users as $user)
							<tr>
								<td class="text-center">{{$user->name}}</td>
							@if ($user->admin)
								<td class="text-center">Admin</td>
							@else
								<td class="text-center">User</td>
							@endif
								<td class="text-center">{{$user->email}}</td>
								<td class="text-center">{{$user->created_at}}</td>
								<td class="text-center">
									<a class="btn btn-primary btn-sm" href="/profile/{{$user->id}}/{{$user->name}}">Profile</a>
									<a class="btn btn-danger btn-sm" href="/admin/{{$user->id}}/delete">Delete</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>

					<div class="container text-center">
						<div class="row">
							{{ $users->links() }}
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection
