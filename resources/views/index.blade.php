@extends('layouts.app')

@section('nav')
  @include('layouts.nav')
@endsection

@section('content')

<div class="container">
    <div class="row">

    @foreach($recipes as $recipe)
        <div class="row">

          <div class="col-md-4">
            <a href="/recipes/{{$recipe->id}}">
            <div class="thumbnail">
              <img class="img-responsive menu-thumbnails" src="{{ asset('/storage/'.$recipe->image) }}"/>
            </div>
            </a>
          </div>

          <div class="col-md-8">
            <h4 class="text-center"><a href="/recipes/{{$recipe->id}}">{{$recipe->title}}</a></h4 >
            <hr>
            <div class="col-md-4">
                <h4>Description:</h4>
                <p>{{ implode(' ', array_slice(explode(' ', strip_tags($recipe->description)), 0, 30)) }} [...]</p>
            </div>
            <div class="col-md-4">
                <h4>Ingredients:</h4>
                <p>{{ implode(' ', array_slice(explode(' ', strip_tags($recipe->ingredients)), 0, 30)) }} [...]</p>
            </div>
            <div class="col-md-4">
              <p>Category: {{ucwords($recipe->category)}}</p>
              <p>Author: {{$recipe->user->name}}</p>
              <hr>
              <p>Date: {{$recipe->created_at->toFormattedDateString()}}</p>
              <a class="btn btn-primary pull-right" href="/recipes/{{$recipe->id}}">Read More</a>
            </div>
          </div>

        </div>
        <hr>
    @endforeach

    </div>
</div>

@endsection
