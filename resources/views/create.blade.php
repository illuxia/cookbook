@extends('layouts.app')

@section('content')

<div class = "container">
  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary">
        <div class="box-header with-border text-center">
            <h4 class="box-title">Create Recipe Form</h4>
        </div>
        <hr>
        <form role="form" action="/recipes" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="5" name="description" required></textarea>
                </div>
                <div class="form-group">
                    <label>Ingredients</label>
                    <textarea class="form-control" rows="10" name="ingredients" required></textarea>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select class="form-control" name="category" required>
                      <option value=""></option>
                      <option value="appetizers">Appetizers</option>
                      <option value="salads">Salads</option>
                      <option value="soups">Soups</option>
                      <option value="main">Main Course</option>
                      <option value="desserts">Desserts</option>
                      <option value="sauces">Sauces</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="image">Upload Image</label>
                    <input type="file" name="image" required>
                </div>
            </div>
            <hr>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                <a href="javascript:history.back()"><button type="button" class="btn btn-default">Cancel</button></a>
            </div>
            <hr>
            @include('layouts.errors')
        </form>
    </div>
  </div>
</div>

@endsection
