@extends('layouts.app')

@section('carousel')
  @include('layouts.carousel')
@endsection

@section('nav')
  @include('layouts.nav')
@endsection

@section('content')

<div class="container">
  <div class="col-md-12 text-center">

    <div class="row">

        <div class="col-sm-12 col-lg-4 col-md-4">
            <div class="thumbnail">
                <img src="images/appetizers.jpg" class="img-responsive">
                <div class="caption">
                    <h4>Appetizers</h4>
                    <p>Total: <span class="badge">{{$count['appetizers']}}</span></p>
                    <p><a href="/recipes/?category=appetizers" class="btn btn-primary">Browse</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-4 col-md-4">
            <div class="thumbnail">
                <img src="images/salads.jpg" class="img-responsive">
                <div class="caption">
                    <h4>Salads</h4>
                    <p>Total: <span class="badge">{{$count['salads']}}</span></p>
                    <p><a href="/recipes/?category=salads" class="btn btn-primary">Browse</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-4 col-md-4">
            <div class="thumbnail">
                <img src="images/soups.jpg" class="img-responsive">
                <div class="caption">
                    <h4>Soups</h4>
                    <p>Total: <span class="badge">{{$count['soups']}}</span></p>
                    <p><a href="/recipes/?category=soups" class="btn btn-primary">Browse</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-4 col-md-4">
            <div class="thumbnail">
                <img src="images/main.jpg" class="img-responsive">
                <div class="caption">
                    <h4>Main Course</h4>
                    <p>Total: <span class="badge">{{$count['main']}}</span></p>
                    <p><a href="/recipes/?category=main" class="btn btn-primary">Browse</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-4 col-md-4">
            <div class="thumbnail">
                <img src="images/desserts.jpg" class="img-responsive">
                <div class="caption">
                    <h4>Desserts</h4>
                    <p>Total: <span class="badge">{{$count['desserts']}}</span></p>
                    <p><a href="/recipes/?category=desserts" class="btn btn-primary">Browse</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-4 col-md-4">
            <div class="thumbnail">
                <img src="images/sauces.jpg" class="img-responsive">
                <div class="caption">
                    <h4>Sauces</h4>
                    <p>Total: <span class="badge">{{$count['sauces']}}</span></p>
                    <p><a href="/recipes/?category=sauces" class="btn btn-primary">Browse</a></p>
                </div>
            </div>
        </div>

    </div>

  </div>
</div>

@endsection
