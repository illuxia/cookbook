<div class="container" style="padding-top: 50px">

  <div class="row">
    <div class="btn-group btn-group-justified" role="group" aria-label="...">

      <div class="btn-group" role="group">
        <a href="/recipes" type="button" class="btn btn-info btn-lg">All Recipes</a>
      </div>

      <div class="btn-group" role="group">
        <a href="/recipes/?category=appetizers" type="button" class="btn btn-info btn-lg">Appetizers</a>
      </div>

      <div class="btn-group" role="group">
        <a href="/recipes/?category=salads" type="button" class="btn btn-info btn-lg">Salads</a>
      </div>

      <div class="btn-group" role="group">
        <a href="/recipes/?category=soups" type="button" class="btn btn-info btn-lg">Soups</a>
      </div>

      <div class="btn-group" role="group">
        <a href="/recipes/?category=main" type="button" class="btn btn-info btn-lg">Main Course</a>
      </div>

      <div class="btn-group" role="group">
        <a href="/recipes/?category=desserts" type="button" class="btn btn-info btn-lg">Desserts</a>
      </div>

      <div class="btn-group" role="group">
        <a href="/recipes/?category=sauces" type="button" class="btn btn-info btn-lg">Sauces</a>
      </div>

    </div>
  </div>

  <hr>

</div>
