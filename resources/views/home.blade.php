@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

          <h3 class="text-center">{{$name}}'s Profile</h3>
          <hr>

            <div class="panel panel-success">
                <div class="panel-heading text-center">Favourite Recipes</div>
                <div class="panel-body">
                  <table class="table table-bordered table-hover">
        						<thead>
        						<tr>
        							<th class="text-center">Title</th>
        							<th class="text-center">Category</th>
                      <th class="text-center">Author</th>
        							<th class="text-center">Actions</th>
        						</tr>
        						</thead>
        						<tbody>
        						@foreach($recipes as $recipe)
        							<tr>
        								<td>{{$recipe->title}}</td>
        								<td class="text-center">{{ucwords($recipe->category)}}</td>
                        <td class="text-center">{{$recipe->user->name}}</td>
        								<td class="text-center">
                          <a class="btn btn-primary btn-sm" href="/recipes/{{$recipe->id}}">View</a>
                          <a class="btn btn-warning btn-sm" href="/recipes/{{$recipe->id}}/remove">Remove</a>
        								</td>
        							</tr>
        						@endforeach
        						</tbody>
        					</table>

                  <div class="container text-center">
        						<div class="row">
        							{{ $recipes->links() }}
        						</div>
        					</div>

                </div>
            </div>

            <div class="panel panel-warning">
                <div class="panel-heading text-center">Comments</div>
                <div class="panel-body">

                      @if ($comments->count())
                        <h4 class="text-center">Total comments: ({{$comments->count()}} )</h4>
                        <hr>
                      @else
                        <h4 class="text-center">No comments yet.</h4>
                        <hr>
                      @endif

                      @foreach ($comments as $comment)
                      <div class="panel panel-info">
                        <div class="panel-heading">
                          <strong>{{$comment->user->name}}</strong>
                          <span class="text-muted">commented on recipe
                            <a href="/recipes/{{$comment->recipe->id}}">{{$comment->recipe->title}}</a>
                            <strong class="pull-right">{{ $comment->created_at->diffForHumans() }}</strong>
                          </span>
                        </div>
                        <div class="panel-body">
                          {{ $comment->body }}
                        </div>
                      </div>
                      @endforeach

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
