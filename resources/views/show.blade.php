@extends('layouts.app')

@section('nav')
  @include('layouts.nav')
@endsection

@section('content')
  <div class="container">
    <div class="row">

        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">{{$recipe->title}}</h3 >
                <hr>
            </div>


            <div class="col-md-4">
                <div class="thumbnail">
                  <img class="img-responsive" src="{{ asset('/storage/'.$recipe->image) }}"/>
                </div>
            </div>


            <div class="col-md-8">
                <div class="col-md-6">
                    <h4>Description:</h4>
                    <p>{{$recipe->description}}</p>
                </div>
                <div class="col-md-6">
                    <h4>Ingredients:</h4>
                    <p>{{$recipe->ingredients}}</p>
                    <hr>
                    <p>Category: {{ucwords($recipe->category)}}</p>
                    <hr>
                  @auth
                    @if(!isset($favoured))
                      <a class="btn btn-success" href="/recipes/{{$recipe->id}}/favourite">Add to Favourites</a>
                    @else
                      <a class="btn btn-danger" href="/recipes/{{$recipe->id}}/remove">Remove from Favourites</a>
                    @endif
                  @endauth
                </div>
            </div>
        </div>

        <hr>

    </div>

    <div class="comments">

      <div class="col-sm-6">
        @if ($recipe->comments()->count())
          <h3>Comments: ({{$recipe->comments()->count()}} )</h3>
          <hr>
        @else
          <h3>No comments yet.</h3>
          <hr>
        @endif

        @foreach ($recipe->comments as $comment)
        <div class="panel panel-warning">
          <div class="panel-heading">
            <strong>{{$comment->user->name}}</strong>
            <span class="text-muted">commented <strong class="pull-right">{{ $comment->created_at->diffForHumans() }}</strong></span>
          </div>
          <div class="panel-body">
            {{ $comment->body }}
          </div>
        </div>
        @endforeach
        <hr>
      </div>

      @auth
      <div class="col-md-8">
        <div class="card">
          <div class="card-block">

            <form action="/recipes/{{$recipe->id}}/comments" method="post">
              {{ csrf_field() }}

              <div class="form-group">
                <textarea name="body" placeholder="Your comment here." class="form-control" rows="8" cols="80"></textarea>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">Add Comment</button>
              </div>
            </form>

          </div>
        </div>
      </div>
      @endauth
    </div>

  </div>
@endsection
