<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Auth::routes();

Route::get('/profile/{id}/{name}', 'HomeController@index');
Route::get('/recipes', 'RecipeController@index');
Route::get('/recipes/create', 'RecipeController@create');
Route::post('/recipes', 'RecipeController@store');
Route::get('/recipes/{recipe}', 'RecipeController@show');
Route::post('/recipes/{recipe}/comments', 'CommentController@store');
Route::get('/search', 'RecipeController@search');
Route::get('/recipes/{recipe}/favourite', 'FavouriteController@store');
Route::get('/favourites', 'FavouriteController@index');
Route::get('/recipes/{recipe}/remove', 'FavouriteController@destroy');
Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/{user}/delete', 'AdminController@destroy');
Route::get('/admin/manage', 'AdminController@manage');
Route::get('/recipe/{recipe}/delete', 'RecipeController@destroy');
Route::get('/admin/{recipe}/edit', 'AdminController@edit');
Route::post('/admin/{recipe}/update', 'AdminController@update');
